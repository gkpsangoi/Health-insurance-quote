<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex">
    <link rel="canonical" href="https://codepen.io/jaycbrf/pen/iBszr/">
    <link rel="stylesheet prefetch" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css">

</head>

<body>

<div class=" well w3-card-4">

    <form class="w3-container" action=" " method="post"  id="survey_form" novalidate="novalidate">

        <fieldset>
            <div class="container">
                <center><h2><legend>Health Insurance Premium Quote generator</legend></h2></center>
            </div>

            <!-- Text input-->
            <div align="center">
                <div class="form-inline w3-text-brown">
                    Name : <div class="form-group">
                                <div class="form-group">
                                    <input  class="w3-input w3-border w3-sand" name="name" placeholder="Name" class="form-control"  type="text">
                                </div>
                            </div>
                </div>
                <!-- Text input-->
                <div class="form-inline w3-text-brown">
                    Gender : <div class="form-group">
                        <div class="input-group">
                            <select class="w3-input w3-border w3-sand" name="gender" placeholder="Gender">
                                <option value="" disabled selected>Select Gender</option>
                                <option value="F">Female</option>
                                <option value="M">Male</option>
                                <option value="M">Other</option>
                            </select>
                        </div>
                </div>
                </div>

                <!-- Text input-->
                <div class="form-inline w3-text-brown">
                    Age : <div class="form-group">
                        <div class="input-group">
                            <input  type="number" class="w3-input w3-border w3-sand" name="age" placeholder="age" class="form-control">
                        </div>
                </div>
                </div>

                <h5 class="w3-text-brown"> Current Health :</h5>

                <!-- radio checks -->
                <div class="form-group">
                        <div class="input-group w3-text-brown">
                                Hypertension : <div class="radio-inline">
                                    <label class="radio-inline">
                                        <input type="radio" name="hypertension" value="Y" /> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="hypertension" value="N" /> No
                                    </label>
                                </div>
                        </div>
                </div>

                <!-- radio checks -->
                <div class="form-group">
                    <div class="input-group w3-text-brown">
                        Blood_Presure : <div class="radio-inline">
                        <label class="radio-inline">
                            <input type="radio" name="blood_presure" value="Y" /> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="blood_presure" value="N" /> No
                        </label>
                    </div>
                    </div>
                </div>

                <!-- radio checks -->
                <div class="form-group">
                        <div class="input-group w3-text-brown">
                                Blood Suger : <div class="radio-inline">
                                    <label class="radio-inline">
                                        <input type="radio" name="blood_suger" value="Y" /> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="blood_suger" value="N" /> No
                                    </label>
                                </div>
                        </div>
                </div>

                <!-- radio checks -->
                <div class="form-group">
                        <div class="input-group w3-text-brown">
                                    Overweight : <div class="radio-inline">
                                    <label class="radio-inline">
                                        <input type="radio" name="overweight" value="Y" /> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="overweight" value="N" /> No
                                    </label>
                                </div>
                        </div>
                </div>

                <h5 class="w3-text-brown"> Habits :</h5>

                <!-- radio checks -->
                <div class="form-group">
                    <div class="input-group w3-text-brown">
                        Smoking : <div class="radio-inline">
                        <label class="radio-inline">
                            <input type="radio" name="smoking" value="Y" /> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="smoking" value="N" /> No
                        </label>
                    </div>
                    </div>
                </div>

                <!-- radio checks -->
                <div class="form-group">
                        <div class="input-group w3-text-brown">
                                    Alcohol : <div class="radio-inline"><label class="radio-inline">
                                        <input type="radio" name="alcohal" value="Y" /> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="alcohal" value="N" /> No
                                    </label>
                                </div>
                        </div>
                </div>

                <!-- radio checks -->
                <div class="form-group">
                        <div class="input-group w3-text-brown">
                                    Daily Exercise :  <div class="radio-inline">
                                    <label class="radio-inline">
                                        <input type="radio" name="dailyexercise" value="Y" /> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="dailyexercise" value="N" /> No
                                    </label>
                                </div>
                        </div>
                </div>

                <!-- radio checks -->
                <div class="form-group">
                    <div class="input-group w3-text-brown">
                                    Drugs : <div class="radio-inline">
                                    <label class="radio-inline">
                                        <input type="radio" name="drugs" value="Y" /> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="drugs" value="N" /> No
                                    </label>
                                </div>
                    </div>
                </div>

            </div>
            <!-- Success message -->
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Insurance Cost</h4>
                        </div>
                        <div id="quotedata" class="modal-body">
                            <p>Some text in the modal.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            <style>.modal-content {
            background-color: #1de9b6;
            }
            .modal-body {
            background-color: #1de9b6;
            }
            .modal-footer{
                background-color: #1de9b6;
            }
            .modal-header{
                background-color: #1de9b6;
            }
            </style>
            <!-- Button -->
            <div class="form-group" align="center">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-warning" id ="submit">Submit<span class=""></span></button>
                </div>
            </div>
            <script src="http://production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js"></script>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
            <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
            <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.4.5/js/bootstrapvalidator.min.js"></script>
            <script>
                $(document).ready(function() {
                    $('#survey_form').bootstrapValidator({
                        feedbackIcons: {
                            valid: 'glyphicon',
                            invalid: 'glyphicon',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                            name: {
                                validators: {
                                    stringLength: {
                                        min: 2,
                                    },
                                    notEmpty: {
                                        message: 'Please enter your name'
                                    }
                                }
                            },
                            gender: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please select your gender'
                                    }
                                }
                            },
                            age: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please enter your age'
                                    }
                                }
                            },

                            hypertension: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please select option'
                                    }
                                }
                            },
                            blood_suger: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please select option'
                                    }
                                }
                            },
                            overweight: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please select option'
                                    }
                                }
                            },
                            alcohal: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please select option'
                                    }
                                }
                            },
                            dailyexercise: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please select option'
                                    }
                                }
                            },
                            drugs: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please select option'
                                    }
                                }
                            },
                            blood_presure: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please select option'
                                    }
                                }
                            },
                            smoking: {
                                validators: {
                                    notEmpty: {
                                        message: 'Please select option'
                                    }
                                }
                            }

                        }
                    })
                });

                $("#submit").click(function() {

                    var name = document.getElementsByName("name")[0].value;
                    var gender = document.getElementsByName("gender")[0].value;
                    var age = document.getElementsByName("age")[0].value;

                    var hypertension = document.getElementsByName("hypertension")[0].value;
                    var blood_presure = document.getElementsByName("blood_presure")[0].value;
                    var blood_suger = document.getElementsByName("blood_suger")[0].value;
                    var overweight = document.getElementsByName("overweight")[0].value;

                    var smoking = document.getElementsByName("smoking")[0].value;
                    var alcohal = document.getElementsByName("alcohal")[0].value;
                    var dailyexercise = document.getElementsByName("dailyexercise")[0].value;
                    var drugs = document.getElementsByName("drugs")[0].value;

                    var surveyFormData={
                                        "name":name,
                                        "gender":gender,
                                        "age":age,

                                        "hypertension":hypertension,
                                        "blod_presure":blood_presure,
                                        "blod_suger":blood_suger,
                                        "overweight":overweight,

                                        "smoking":smoking,
                                        "alcohal":alcohal,
                                        "dailyexercise":dailyexercise,
                                        "drugs":drugs
                                        }

                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        type: "post",
                        dataType: "json",
                        data:JSON.stringify(surveyFormData),
                        url: "http://localhost:8080/health-insurance-premium-quote-generator/QuoteHandler/getQuote",
                        success: function(data){
                            if(data.res){
                                $('#quotedata').html(data.res);
                                $('#myModal').modal('show');
                            }

                        }
                    });
                });


            </script>
        </fieldset>

    </form>
</div>

</body>
</html>
