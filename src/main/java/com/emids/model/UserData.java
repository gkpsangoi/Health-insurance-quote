package com.emids.model;

public class UserData {

	private String name;
	private String gender;
	private String age; 
	private String hypertension;
	private String blod_presure;
	private String blod_suger;
	private String overweight;
	private String smoking;
	private String alcohal;
	private String dailyexercise;
	private String drugs;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getBlod_presure() {
		return blod_presure;
	}
	public void setBlod_presure(String blod_presure) {
		this.blod_presure = blod_presure;
	}
	public String getBlod_suger() {
		return blod_suger;
	}
	public void setBlod_suger(String blod_suger) {
		this.blod_suger = blod_suger;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohal() {
		return alcohal;
	}
	public void setAlcohal(String alcohal) {
		this.alcohal = alcohal;
	}
	public String getDailyexercise() {
		return dailyexercise;
	}
	public void setDailyexercise(String dailyexercise) {
		this.dailyexercise = dailyexercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	@Override
	public String toString() {
		return "UserData [name=" + name + ", gender=" + gender + ", age=" + age + ", hypertension=" + hypertension
				+ ", blod_presure=" + blod_presure + ", blod_suger=" + blod_suger + ", overweight=" + overweight
				+ ", smoking=" + smoking + ", alcohal=" + alcohal + ", dailyexercise=" + dailyexercise + ", drugs="
				+ drugs + "]";
	}
	
	
}
