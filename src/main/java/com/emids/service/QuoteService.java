package com.emids.service;

import java.text.DecimalFormat;

import com.emids.model.UserData;
import com.emids.serviceDAO.QuoteServiceDAO;

public class QuoteService  implements QuoteServiceDAO{

	
	@Override
	public String getQuote(UserData userData){
		
		String result=null;
		
		try{
		double premium=5000;		
		
		
		String name    			=	userData.getName();
		String gender			=	userData.getGender();
		int age					=	Integer.parseInt(userData.getAge());
		String hypertension		=	userData.getHypertension();
		String blod_presure		=	userData.getBlod_presure();
		String blod_suger		=	userData.getBlod_suger();
		String overweight		=	userData.getOverweight();
		String smoking			=	userData.getSmoking();
		String alcohal			=	userData.getAlcohal();
		String dailyexercise	=	userData.getDailyexercise();
		String drugs			=	userData.getDrugs();
			
		int agecnt=0; 
		
		
		  if(age<18){
			  
			  agecnt=1;
		  }
		  if(age>=18&&age<25){
			  
			  agecnt=2;
		  }
		  if(age>=25&&age<30){
			  
			  agecnt=3;
		  }
		  if(age>=30&&age<35){
		  
		  agecnt=4;
		  }
		  if(age>=35&&age<40){
			  
			  agecnt=5;
			  }
		  if(age>=40){
			agecnt=6;
			}
			  

			switch(agecnt){
			case 6://do nothing
			case 5:premium+=premium*.1;
			case 4:premium+=premium*.1;
			case 3:premium+=premium*.1;
			case 2:premium+=premium*.1;
						
			}
			
			if(age>=40){
			int agediff=age-40;
			int periodOf5=(agediff/5);
			for(int i=0;i<=periodOf5;i++){
			premium+=premium*.2;
			}
		}
	       
			if(gender.equals("m")){
				
				premium+=premium*.02;
			}
			
			
			//code for Pre_existing conditions 
			
			double increments_Pre_existing=0;
			
			if(hypertension.equalsIgnoreCase("y")){
				
				increments_Pre_existing+=.01;
				//premium+=premium*.01;
			}
			if(blod_suger.equalsIgnoreCase("y")){
				
				increments_Pre_existing+=.01;
				//premium+=premium*.01;
			}
			if(overweight.equalsIgnoreCase("y")){
		
				increments_Pre_existing+=.01;
			//	premium+=premium*.01;
			}
			if(blod_presure.equalsIgnoreCase("y")){
		
				increments_Pre_existing+=.01;
				//premium+=premium*.01;
			}
			
			
			premium+=premium*increments_Pre_existing;
			
			
			
			//code for habits
			double increments_habits=0;
			if(smoking.equalsIgnoreCase("y")){
				
				increments_habits+=.03;
				//premium+=premium*.03;

			}
			
			
			if(alcohal.equalsIgnoreCase("y")){
				
				increments_habits+=.03;
				//premium+=premium*.03;

			}
			
			if(drugs.equalsIgnoreCase("y")){
				
				increments_habits+=.03;

				//premium+=premium*.03;

			}
			
			if(dailyexercise.equalsIgnoreCase("y")){
				
				increments_habits-=.03;

				//premium-=premium*.03;

			}
			
			
			DecimalFormat formatter = new DecimalFormat("#,###");

			System.out.println(formatter.format(Math.ceil(premium)));
	    result="Health Insurance Premium for Mr. "+name+": Rs."+formatter.format(Math.ceil(premium));
		}catch(Exception e){
			e.printStackTrace();
			
		}
			
		return result;
		
	}
	
	
}
