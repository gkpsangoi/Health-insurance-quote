package com.emids.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.emids.model.UserData;
import com.emids.service.QuoteService;

import junit.framework.TestCase;

public class QuoteTest {

	
	@Test
	public void getQuote(){
		
		QuoteService quoteService=new QuoteService();
		
		UserData userData=new UserData ();
		userData.setName("Govind Kumar");
		userData.setGender("m");
		userData.setAge("34");
		userData.setHypertension("n");
		userData.setAlcohal("y");
		userData.setOverweight("n");
		userData.setBlod_presure("y");
		userData.setBlod_suger("n");
		userData.setDailyexercise("y");
		userData.setDrugs("y");
		userData.setSmoking("y");
		String result=quoteService.getQuote(userData);
		
		
		assertEquals(result,"Health Insurance Premium for Mr. Govind Kumar: Rs.6,856");
		
	}
	
	
}
